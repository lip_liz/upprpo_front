import {PerfumePrice} from "../../types/types";

export const perfumer: Array<{ name: string }> = [
    {"name": "Burberry"},
    {"name": "Bvlgari"},
    {"name": "Calvin Klein"},
    {"name": "Carolina Herrera"},
    {"name": "Chanel"},
    {"name": "Creed"},
    {"name": "Dior"},
    {"name": "Dolce&Gabbana"},
    {"name": "Giorgio Armani"},
    {"name": "Gucci"},
    {"name": "Hermes"},
    {"name": "Hugo Boss"},
    {"name": "Jean Paul Gaultier"},
    {"name": "Lancome"},
    {"name": "Paco Rabanne"},
    {"name": "Prada"},
    {"name": "Tom Ford"},
    {"name": "Versace"}
];

export const gender: Array<{ name: string }> = [
    {"name": "male"},
    {"name": "female"}
];

export const price: Array<PerfumePrice> = [
    {"id": 1, "name": "any", "array": []},
    {"id": 2, "name": "15 - 25 $", "array": [15, 25]},
    {"id": 3, "name": "25 - 40 $", "array": [25, 40]},
    {"id": 4, "name": "40 - 90 $", "array": [40, 90]},
    {"id": 5, "name": "90 - 175+ $", "array": [90, 250]}
];

export const volume: Array<{ name: string }> = [
    {"name": "40"},
    {"name": "50"},
    {"name": "75"},
    {"name": "80"},
    {"name": "90"},
    {"name": "100"},
    {"name": "125"},
    {"name": "150"},
    {"name": "200"}
];

export const type: Array<{ name: string }> = [
    {"name": "Eau de parfum"},
    {"name": "Eau de Toilette"}
];

export const year: Array<{ name: string }> = [
    {"name": "1921"},
    {"name": "1987"},
    {"name": "1988"},
    {"name": "1990"},
    {"name": "1993"},
    {"name": "1994"},
    {"name": "1995"},
    {"name": "1996"},
    {"name": "1997"},
    {"name": "1998"},
    {"name": "1999"},
    {"name": "2000"},
    {"name": "2001"},
    {"name": "2002"},
    {"name": "2003"},
    {"name": "2004"},
    {"name": "2005"},
    {"name": "2006"},
    {"name": "2007"},
    {"name": "2008"},
    {"name": "2009"},
    {"name": "2010"},
    {"name": "2011"},
    {"name": "2012"},
    {"name": "2013"},
    {"name": "2014"},
    {"name": "2015"},
    {"name": "2016"},
    {"name": "2017"},
    {"name": "2018"},
    {"name": "2019"}
];

