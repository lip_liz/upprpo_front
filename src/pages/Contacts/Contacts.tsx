import React, {FC} from 'react';
import {faInfoCircle} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

const Contacts: FC = () => {
    return (
        <div className="container mt-5">
            <h4><FontAwesomeIcon className="ml-2 mr-2" icon={faInfoCircle}/>Contacts</h4>
            <p></p>
            <p>Липчинская Елизавета, 19211</p>
            <p>Евсюков Анатолий, 19211</p>
            <p>Базаров Андрей, 19211</p>
            <p>Уточкин Станислав, 19211</p>
            <p>Пашенцев Марат, 19211</p>
        </div>
    );
};

export default Contacts