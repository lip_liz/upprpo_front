import React, {FC, ReactElement} from 'react';

import "./Footer.css";

const Footer: FC = (): ReactElement => {
    return (
        <footer className="page-footer p-5 text-black">
            <div className="container">
                <div className="mx-auto" style={{textAlign:"center"}}>
                    <p>Copyright &copy; 2022</p>
                </div>
            </div>
        </footer>
    );
}

export default Footer