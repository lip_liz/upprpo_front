export const validateEmail = (email: string | undefined): string => {
    if (email === undefined || !email.match(/^(\D)+(\w)*((\.(\w)+)?)+@(\D)+(\w)*((\.(\D)+(\w)*)+)?(\.)[a-z]{2,}$/)) {
        return "Not a valid Email";
    }

    return "";
};