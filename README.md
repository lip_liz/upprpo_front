## 1. How to build a project
+ Клонировать репозиторий и собрать проект с помощью менеджера пакетов npm (**npm install/ci**, **npm start**)
+ [Скачать](https://disk.yandex.ru/client/disk/build) (`проект`) запустить через команду **npm run build**
## 2. Instruments
+ React
+ TypeScript
+ Bootstrap
+ Redux
+ GraphQL
+ Axios
## 3. Usage
Чтобы получить все возможности приложения, необходимо собрать [backend-проект](https://gitlab.com/s.utochkin/upprpo) и запустить одновременно с данным приложением
## 4. Authors 
**Липчинская Елизавета** 19211 (Backend)

**Евсюков Анатолий** 19211 (DevOps)

**Базаров Андрей** 19211 (Frontend)

**Уточкин Станислав** 19211 (Backend)

**Пашенцев Марат** 19211 (Backend)
